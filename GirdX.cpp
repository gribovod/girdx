#include "GirdX.h"

void GirdX::breakCowl() {
	currentState_ = GirdState::Parachute;
	SetEmptyMass(26.5);

	VESSELSTATUS vs;
	this->GetStatus(vs);
		vs.arot.y -= 3.1416 / 2;
	oapiCreateVessel("rightCowl", "GirdXCowl", vs);
		vs.arot.y += 3.1416 ;
	oapiCreateVessel("leftCowl", "GirdXCowl", vs);

	ClearMeshes(false);
	AddMesh("GirdXReturn");
}

void GirdX::clbkPreStep(double simt, double simdt, double mjd)
{
	if (currentState_ == GirdState::IamCowl) {
		return;
	}
/*	if (currentState_ == GirdState::CowlTime) {
		auto currTime = std::chrono::system_clock::now();
		auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(currTime - parachuteTimer_);
		if (elapsed.count() > 5) {
			breakCowl();
		}
	}
	if (currentState_ == GirdState::Active) {
		currentState_ = GirdState::CowlTime;
		parachuteTimer_ = std::chrono::system_clock::now();
	}*/

	if (currentState_ == GirdState::Active && this->GetPropellantMass(ph_main_) <= DBL_EPSILON) {
		currentState_ = GirdState::CowlTime;
		parachuteTimer_ = oapiGetSimTime();
	}

	if (currentState_ == GirdState::CowlTime) {
		auto currTime = oapiGetSimTime();
		if (currTime - parachuteTimer_ > 13) {
			breakCowl();
		}
	}
}

//double aoa, double M, double Re,double *cl, double *cm, double *cd
void vlift(double aoa, double M, double Re, double *cl, double *cm, double *cd)
{
	static const double clp[] = {  // lift coefficient from -pi to pi in 10deg steps
		-0.1,-0.5,-0.4,-0.1,0,0,0,0,0,0,0,0,0,0,-0.2,-0.6,-0.6,-0.4,0.2,0.5,0.9,0.8,0.2,0,0,0,0,0,0,0,0,0,0.1,0.4,0.5,0.3,-0.1,-0.5
	};
	static const double aoa_step = 10.0*RAD;
	double a, fidx, saoa = sin(aoa);
	a = modf((aoa + PI) / aoa_step, &fidx);
	int idx = (int)(fidx + 0.5);
	*cl = clp[idx] * (1.0 - a) + clp[idx + 1] * a;     // linear interpolation
	*cm = 0.0; //-0.03*sin(aoa-0.1);
	*cd = 0.03 + 0.4*saoa*saoa;                // profile drag
	*cd += oapiGetInducedDrag(*cl, 1.0, 0.5); // induced drag
	*cd += oapiGetWaveDrag(M, 0.75, 1.0, 1.1, 0.04);  // wave drag
}

void GirdX::clbkSetClassCaps(FILEHANDLE cfg)
{
	char pszBuffer[80];
	if (oapiReadItem_string(cfg, "ClassName", pszBuffer))
	{
		if (!_strnicmp(pszBuffer, "Cowl", 4))
		{
			currentState_ = GirdState::IamCowl;
		}
	}
	SetGravityGradientDamping(20.0);
	if (currentState_ == GirdState::IamCowl) {
		SetSize(0.4);
		SetEmptyMass(0.2);
		SetPMI(_V(0.00245, 0.00245, 0.00245));

		// airfoil definitions
		const VECTOR3 PB_COP = { 0, -0.5, 0 };      // centre of pressure for airfoils [m]
		CreateAirfoil(LIFT_VERTICAL, PB_COP, vlift, 1.1, 0.132, 4);

		// Define impact convex hull
		static const DWORD ntdvtx = 4;
		static const TOUCHDOWNVTX tdvtx[ntdvtx] = {
			{_V(-0.07,  0.00,  0.00), 2e4, 1e3, 1.6, 1},
			{_V( 0.00,  0.35,  0.00), 2e4, 1e3, 1.6, 1},
			{_V( 0.07,  0.00,  0.00), 2e4, 1e3, 1.6, 1},
			{_V( 0.00,  0.00,  0.07), 2e4, 1e3, 1.6, 1}
		};
		SetTouchdownPoints(tdvtx, ntdvtx);
	//	AddForce(_V(1, 0, 0), _V(0, 1, 0));
	}
	else {
		SetSize(1.1);
		SetEmptyMass(29.5);
		SetPMI(_V(0.40456, 0.00245, 0.40456));

		// airfoil definitions
		const VECTOR3 PB_COP = { 0, -0.5, 0 };      // centre of pressure for airfoils [m]
		CreateAirfoil(LIFT_VERTICAL, PB_COP, vlift, 1.1, 0.132, 4);

		// Define impact convex hull
		static const DWORD ntdvtx = 5;
		static const TOUCHDOWNVTX tdvtx[ntdvtx] = {
			{_V(0.19, -1.10,  0.00), 2e4, 1e3, 1.6, 1},
			{_V(0.00, -1.10,  0.19), 2e4, 1e3, 1.6, 1},
			{_V(-0.19, -1.10,  0.00), 2e4, 1e3, 1.6, 1},
			{_V(0.00, -1.10, -0.19), 2e4, 1e3, 1.6, 1},
			{_V(0.00,  1.10, 0.00), 2e4, 1e3, 3.0}
		};
		SetTouchdownPoints(tdvtx, ntdvtx);

		const double MAX_MAIN_FUEL = 8.3;
		ph_main_ = CreatePropellantResource(MAX_MAIN_FUEL);
		const double TO_N = 3.2 * 9.81;
		const double MAX_MAIN_THRUST = 70 * TO_N;
		const double VAC_MAIN_ISP = 175 * TO_N;
		const double NML_MAIN_ISP = 162 * TO_N;
		th_main_ = CreateThruster(_V(0, -0.92, 0), _V(0, 1, 0), MAX_MAIN_THRUST, ph_main_, VAC_MAIN_ISP, NML_MAIN_ISP);
		CreateThrusterGroup(&th_main_, 1, THGROUP_MAIN);

		EXHAUSTSPEC es_main = { th_main_, NULL, NULL, NULL, 1.1, 0.07, 0, 0.1, NULL };
		AddExhaust(&es_main);
		PARTICLESTREAMSPEC exhaust_main = {
			0, 0.07, 13, 15, 0, 0.1, 5, 1.0, PARTICLESTREAMSPEC::DIFFUSE,
			PARTICLESTREAMSPEC::LVL_SQRT, 0, 1,
			PARTICLESTREAMSPEC::ATM_PLOG, 1e-5, 0.1
		};
		AddExhaustStream(th_main_, _V(0, -0.92, 0), &exhaust_main);
	}
}

void GirdX::clbkLoadStateEx(FILEHANDLE scn, void *status)
{
	char *line;
	while (oapiReadScenario_nextline(scn, line)) 
	{ 
	/*	if (!_strnicmp(line, "GSTATE", 6)) 
		{
			sscanf(line + 6, "%d", &currentState_);
		} else*/ { 
			ParseScenarioLineEx(line, status);
		} 
	} 
} 

void GirdX::clbkSaveState(FILEHANDLE scn)
{
	VESSEL2::clbkSaveState(scn);
	oapiWriteScenario_int(scn, "GSTATE", (int)currentState_);
}

GirdX::GirdX(OBJHANDLE hObj, int fmodel) 
	: VESSEL3(hObj, fmodel)
	, currentState_(GirdState::Active)
{
}

GirdX::~GirdX()
{
}
