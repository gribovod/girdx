#pragma once

#define STRICT 1
#include "orbitersdk.h"

enum class GirdState {
	Active = 1,
	CowlTime,
	Parachute,
	IamCowl,
};

class GirdX :	public VESSEL3
{

public:
	void breakCowl();
	void clbkPreStep(double simt, double simdt, double mjd) override;
	void clbkSetClassCaps(FILEHANDLE cfg) override;
	void clbkLoadStateEx(FILEHANDLE scn, void *status) override; 
	void clbkSaveState(FILEHANDLE scn) override;
	GirdX(OBJHANDLE hObj, int fmodel);
	~GirdX();
private:
	double parachuteTimer_;
	GirdState currentState_;
	PROPELLANT_HANDLE ph_main_;
	THRUSTER_HANDLE th_main_;
};

