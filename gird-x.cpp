﻿

#define ORBITER_MODULE

//#include "OrbiterAPI.h"
#include "orbitersdk.h"
#include "GirdX.h"

HINSTANCE g_hDLL;

DLLCLBK void InitModule(HINSTANCE hModule)
{
	g_hDLL = hModule;
}

DLLCLBK void ExitModule(HINSTANCE hModule)
{
	// perform module cleanup here
}

DLLCLBK VESSEL *ovcInit(OBJHANDLE hvessel, int flightmodel) 
{
	return new GirdX(hvessel, flightmodel); 
}

DLLCLBK void ovcExit(VESSEL *vessel) 
{ 
	if (vessel) 
		delete (GirdX*)vessel;
}
